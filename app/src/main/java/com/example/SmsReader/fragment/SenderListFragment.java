package com.example.SmsReader.fragment;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ListView;
import com.example.SmsReader.activity.MessagesListActivity;
import com.example.SmsReader.adapter.SenderListAdapter;
import com.example.SmsReader.model.Message;
import com.example.SmsReader.model.Sender;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Rajesh on 9/18/2014.
 */
public class SenderListFragment extends ListFragment
    implements LoaderManager.LoaderCallbacks<Cursor> {

  public static final int LOADER_ID = 1;
  public static final String STATE_LISTVIEW_POS = "listview_pos_state";
  private static final String LOG_TAG = "SMS_MESSAGES";
  private static final String STATE_LISTVIEW_DATA = "listview_data_state";

  private SenderListAdapter mListAdapter;
  private int mListPosition = -1;
  private ArrayList<Sender> mSenders;

  public SenderListFragment() {
  }

  public static SenderListFragment getInstance(Bundle args) {
    SenderListFragment senderListFragment = new SenderListFragment();
    senderListFragment.setArguments(args);
    return senderListFragment;
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    init(savedInstanceState);
  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    Sender sender = (Sender) mListAdapter.getItem(position);
    MessagesListActivity.startMessageListActivity(sender, getActivity());
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    int position = getListView().getFirstVisiblePosition();
    outState.putInt(STATE_LISTVIEW_POS, position);
    outState.putParcelableArrayList(STATE_LISTVIEW_DATA, mSenders);
  }

  private void init(Bundle state) {
    mListAdapter = new SenderListAdapter(Collections.EMPTY_LIST);
    setListAdapter(mListAdapter);
    if (state != null) {
      if (state.containsKey(STATE_LISTVIEW_DATA)) {
        mSenders = state.getParcelableArrayList(STATE_LISTVIEW_DATA);
        mListAdapter.setSenders(mSenders);
        if (state.containsKey(STATE_LISTVIEW_POS)) {
          mListPosition = state.getInt(STATE_LISTVIEW_POS);
        }
      }
    } else {
      LoaderManager loaderManager = getActivity().getSupportLoaderManager();
      Loader<Cursor> loader = loaderManager.initLoader(LOADER_ID, null, this);
      loader.startLoading();
    }
  }

  @Override
  public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
    // I couldn't find the way to group sms by address, so if manually grouping it.
    Uri allMessages = Uri.parse("content://sms/");
    String projection[] = { "_id", "address", "date", "date_sent", "body", "read" };
    return new CursorLoader(getActivity(), allMessages, projection, null, null, "date asc");
  }

  @Override
  public void onLoadFinished(Loader<Cursor> objectLoader, Cursor cursor) {
    //FIXME This should be done in background thread
    mSenders = new ArrayList<Sender>();

    while (cursor.moveToNext()) {
      Message message = Message.from(cursor);
      Sender sender = new Sender(message.getAddress());
      //Note: Sender's equals and hashCode methods are overriden
      int index = mSenders.indexOf(sender);
      if (index >= 0) {
        sender = mSenders.get(index);
      } else {
        mSenders.add(sender);
      }
      sender.add(message);
      if (message.getDate() > sender.getDate()) {
        sender.setDate(message.getDate());
      }
    }
    Collections.sort(mSenders);
    mListAdapter.setSenders(mSenders);
    mListAdapter.notifyDataSetChanged();
    setListShown(true);
    if (mListPosition != -1) {
      getListView().setSelection(mListPosition);
    }
  }

  @Override
  public void onLoaderReset(Loader<Cursor> objectLoader) {
  }
}
