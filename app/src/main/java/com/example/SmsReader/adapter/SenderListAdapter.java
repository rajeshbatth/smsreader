package com.example.SmsReader.adapter;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.SmsReader.R;
import com.example.SmsReader.model.Sender;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rajesh on 9/18/2014.
 */
public class SenderListAdapter extends BaseAdapter {

  private List<Sender> mMessageList;

  public SenderListAdapter(List<Sender> senders) {
    this.mMessageList = senders;
  }

  @Override
  public int getCount() {
    return mMessageList.size();
  }

  @Override
  public Object getItem(int position) {
    return mMessageList.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    Context context = parent.getContext();
    if (convertView == null) {
      convertView = View.inflate(context, R.layout.sender_list_item, null);
    }

    Sender sender = mMessageList.get(position);
    TextView senderNameTxtView = (TextView) convertView.findViewById(R.id.sender_name);
    senderNameTxtView.setText(sender.getName());

    TextView sentDateTxtView = (TextView) convertView.findViewById(R.id.sent_date);
    CharSequence timeString =
        DateUtils.getRelativeTimeSpanString(sender.getDate(), System.currentTimeMillis(),
            60 * 60 * 1000, DateUtils.FORMAT_ABBREV_TIME);
    if (timeString != null) {
      sentDateTxtView.setText(timeString);
    }

    return convertView;
  }

  public void setSenders(ArrayList<Sender> list) {
    mMessageList = list;
  }
}
