package com.example.SmsReader.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.SmsReader.R;
import com.example.SmsReader.model.Message;
import com.example.SmsReader.model.Sender;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Rajesh on 9/18/2014.
 */
public class MessageListAdapter extends BaseAdapter {

  private final DateFormat mDateFormat;
  private List<Message> mMessageList;

  public MessageListAdapter(Sender sender) {
    mMessageList = sender.getMessages();
    mDateFormat = new SimpleDateFormat("HH:MM, MMM d");
  }

  @Override
  public int getCount() {
    return mMessageList.size();
  }

  @Override
  public Object getItem(int position) {
    return mMessageList.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    Context context = parent.getContext();
    if (convertView == null) {
      convertView = View.inflate(context, R.layout.message_list_item, null);
    }

    Message message = mMessageList.get(position);
    TextView senderNameTxtView = (TextView) convertView.findViewById(R.id.msg_body);
    senderNameTxtView.setText(message.getBody());
    if (!message.isRead()) {
      senderNameTxtView.setTypeface(Typeface.DEFAULT_BOLD);
    } else {
      senderNameTxtView.setTypeface(Typeface.DEFAULT);
    }

    TextView sentDateTxtView = (TextView) convertView.findViewById(R.id.sent_date);
    String sentTime = mDateFormat.format(new Date(message.getDate()));
    sentDateTxtView.setText(sentTime);

    return convertView;
  }

  public void setSenders(List<Message> list) {
    mMessageList = list;
  }
}
