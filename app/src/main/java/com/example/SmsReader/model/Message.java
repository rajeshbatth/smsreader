package com.example.SmsReader.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rajesh on 9/18/2014.
 */
public class Message implements Parcelable {

  public static final Creator<Message> CREATOR = new Creator<Message>() {
    public Message createFromParcel(Parcel source) {
      return new Message(source);
    }

    public Message[] newArray(int size) {
      return new Message[size];
    }
  };

  private int id;
  private String address;
  private long date;
  private long dateSent;
  private String body;
  private boolean isRead;

  public Message() {
  }

  private Message(Parcel in) {
    this.id = in.readInt();
    this.address = in.readString();
    this.date = in.readLong();
    this.dateSent = in.readLong();
    this.body = in.readString();
    this.isRead = in.readByte() != 0;
  }

  public static Message from(Cursor cursor) {
    if (cursor == null || cursor.isClosed()) {
      return null;
    }

    //For performance sake not using setters and getters
    Message message = new Message();
    message.id = cursor.getInt(cursor.getColumnIndex("_id"));
    message.address = cursor.getString(cursor.getColumnIndex("address"));
    message.date = cursor.getLong(cursor.getColumnIndex("date"));
    message.dateSent = cursor.getLong(cursor.getColumnIndex("date_sent"));
    message.body = cursor.getString(cursor.getColumnIndex("body"));
    message.isRead = cursor.getInt(cursor.getColumnIndex("read")) != 0;
    return message;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public long getDate() {
    return date;
  }

  public void setDate(long date) {
    this.date = date;
  }

  public long getDateSent() {
    return dateSent;
  }

  public void setDateSent(long dateSent) {
    this.dateSent = dateSent;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public boolean isRead() {
    return isRead;
  }

  public void setRead(boolean isRead) {
    this.isRead = isRead;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(this.id);
    dest.writeString(this.address);
    dest.writeLong(date);
    dest.writeLong(dateSent);
    dest.writeString(this.body);
    dest.writeByte(isRead ? (byte) 1 : (byte) 0);
  }
}
