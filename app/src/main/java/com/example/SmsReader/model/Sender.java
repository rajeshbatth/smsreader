package com.example.SmsReader.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rajesh on 9/18/2014.
 */
public class Sender implements Parcelable, Comparable<Sender> {

  public static final Creator<Sender> CREATOR = new Creator<Sender>() {
    public Sender createFromParcel(Parcel source) {
      return new Sender(source);
    }

    public Sender[] newArray(int size) {
      return new Sender[size];
    }
  };

  private String name;
  private long date;
  private List<Message> messages = new ArrayList<Message>();

  public Sender(String name) {
    this.name = name;
  }

  public Sender() {
  }

  private Sender(Parcel in) {
    this.name = in.readString();
    this.date = in.readLong();
    in.readTypedList(messages, Message.CREATOR);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getDate() {
    return date;
  }

  public void setDate(long date) {
    this.date = date;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || !(o instanceof Sender)) {
      return false;
    }

    Sender sender = (Sender) o;

    if (!name.equals(sender.name)) return false;

    return true;
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public int compareTo(Sender another) {
    if (another.getDate() > date) {
      return 1;
    } else if (another.getDate() < date) {
      return -1;
    }
    return 0;
  }

  public void add(Message message) {
    messages.add(message);
  }

  public List<Message> getMessages() {
    return messages;
  }

  public void setMessages(List<Message> messages) {
    this.messages = messages;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.name);
    dest.writeLong(this.date);
    dest.writeTypedList(messages);
  }
}
