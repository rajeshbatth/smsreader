package com.example.SmsReader.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import com.example.SmsReader.R;
import com.example.SmsReader.adapter.MessageListAdapter;
import com.example.SmsReader.model.Sender;

public class MessagesListActivity extends Activity {

  public static final String EXTRA_SENDER = "extra_sender";

  private ListView mListView;

  public static void startMessageListActivity(Sender sender, Context context) {
    Intent intent = new Intent(context, MessagesListActivity.class);
    intent.putExtra(EXTRA_SENDER, sender);
    context.startActivity(intent);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_messages_list);
    mListView = (ListView) findViewById(R.id.msg_list);
    Bundle extras = getIntent().getExtras();
    if (extras.containsKey(EXTRA_SENDER)) {
      Sender sender = extras.getParcelable(EXTRA_SENDER);
      init(sender);
    }
  }

  private void init(Sender sender) {
    setTitle(sender.getName());
    mListView.setAdapter(new MessageListAdapter(sender));
    mListView.setStackFromBottom(true);
    mListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.messages_list, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.toggle_stack_pos:
        boolean stackFromBottom = mListView.isStackFromBottom();
        mListView.setStackFromBottom(!stackFromBottom);
        item.setTitle(stackFromBottom ? "Bottom" : "Top");
        break;
    }
    return super.onOptionsItemSelected(item);
  }
}
